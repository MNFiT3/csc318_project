<!-- Created by ZeD, MN-FiT3 -->
<?php 
        session_start();
        $_SESSION['usrID'] = "Student";

        include_once "assets/php/dbc.php";
        $conn = connectDB();
        $sql = "SELECT * FROM `item_lists`";
        $result = SQL($sql, $conn, "GET");

        $out = NULL;
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $data = array($row['ITEM_LISTS_ID'], $row['ITEM_LISTS_NAME']);
                $out = $out . "<option value='$data[0]'>$data[1]</option>";
            }
        }
        closeDB($conn);
    ?>
<html>
    <head>
        <script src="assets/custom/upk-ss.js"></script>
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
    </head>
     <body>
        <nav class="navbar navbar-inverse bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">UPK Storage System</a>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="index.html" style="color:white">Homepage</a>
                </li>
            </ul>
        </div>
        </nav>
        <!-- Content Start Here -->
        <form name="form1" id="form1" method="post" action="assets/php/data.php">
        <div class="container">
            <div>
            <br>
            <h1>Student Details</h1>
            <hr />
            <div class="form-group">
                <label>Student ID :</label>
                <input name="studID" type="text" class="form-control" autocomplete="off" required placeholder = "Student ID" onkeypress="return validateStudID(event)" maxlength="10"> 
            </div>
            <div class="form-group">
                <label>Student Name :</label>
                <input name="studNme" type="text" class="form-control" autocomplete="off" required placeholder = "Enter your name" onkeypress="return validateStudName(event)"> 
            </div>
            <div class="form-group">
                <label>Student Email :</label>
                <input name="studEmail" type="email" class="form-control" autocomplete="off" required placeholder = "Enter your email"> 
            </div>
                <div class="form-group">
                <label>Student Phone No :</label>
                <input name="studPhone" type="text" class="form-control" autocomplete="off" required placeholder = "Phone Number" onkeypress="return validateStudID(event)"> 
            </div>
        </div>
        <br />
        <hr />
        <div>
            <br>
            <h1>Items Details</h1>
            <hr />
            <div class="row">
                <div class="form-group col-md">
                    <label>Select item 1</label>
                    <select name="item1" class="form-control">
                        <?php echo $out ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md">
                    <label>Select item 2</label>
                    <select name="item2" class="form-control">
                        <?php echo $out ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md">
                    <label>Select item 3</label>
                    <select name="item3" class="form-control">
                        <?php echo $out ?>
                    </select>
                </div>
            </div>
            <br />
            <input name="sentDat" value="StudAddItem" hidden>
            <button type="button" class="btn btn-info" onclick="checkForm('form1')">Proceed</button>
        </div>
            </div>
        </form>
        <!-- Content End Here -->
        <script src="assets/plugins/jquery-3.2.1.min.js"></script>
        <script src="assets/js/tether.min.js"></script>
        <script src="assets/js/bootstrap.js"></script>
        
        
    </body>
</html>