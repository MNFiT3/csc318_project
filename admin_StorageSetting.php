<!-- Created by MN-FiT3 -->
<?php include_once ("assets/php/session.php"); IsStudent(); ?>
        <div><!-- Content Start Here -->
            <form name="adminSetting" method="post" action="assets/php/data.php">
                <div class="form-group">
                    <label>Storage Name</label>
                    <input name="storageName" type="text" maxlength="16" placeholder="Storage Name" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Date to Stored</label>
                    <input name="dateStart" type="date" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Date to Returned</label>
                    <input name="dateEnd" type="date" class="form-control" required>
                </div>
                <button name="submit" type="submit" value="adminSetting" class="btn btn-success">Update</button>
            </form>
        </div><!-- Content End Here -->