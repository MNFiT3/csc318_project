function validatePass(eID1,eID2){
    (document.getElementById(eID1).value != document.getElementById(eID2).value) ? alert("Password Did Not Match!") : checkForm("form1");
}
function checkForm(formID){
    var form = document.getElementById(formID);
    form.checkValidity() ? form.submit() : alert("Error in one or more form!");
}
function validateStudName(eEVT){
    return eEVT.charCode >= 65 && eEVT.charCode <= 90 || eEVT.charCode == 20 || eEVT.charCode == 32 || eEVT.charCode >= 97 && eEVT.charCode <= 122 || eEVT.charCode == 64;
}
function validateStudID(eEVT){
    return eEVT.charCode >= 48 && eEVT.charCode <= 57;
}