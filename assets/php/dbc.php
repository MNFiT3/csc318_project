<?php

    function connectDB(){
        include "config.php";

        $conn = false;
        
        // Create connection
        $conn = new mysqli($DB_servername, $DB_username, $DB_password, $DB_name);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }else{
            //echo "Connected successfully";
        }
        return $conn;
    }
    
    function closeDB($conn){
        $conn->close();
    }

    //Using "GET" / "SET" flag to prevent error message
    function SQL($sql,$conn,$method){
        $result = NULL;
        if($method == "GET"){
            $result = $conn->query($sql);
        }elseif($method == "SET"){
            $result = $conn->query($sql);
            
            if ($result === TRUE) {
                //echo "Message [dbc.php]: New record created successfully";
            } else {
                echo "Error [dbc.php]: " . $sql . "<br>" . $conn->error;
            }
        }

        return $result;
    }

    function duplicateCheck($sql,$conn){
         $result = SQL($sql,$conn,"GET");
         if ($result->num_rows > 0) {
             return false;
         }else{
             return true;
         }
    }

    /*  This is for retriving data
        if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";
            }
        }  else {
            echo "0 results";
        }

    */
?>