<?php
    include_once ("session.php");
    include_once ("dbc.php");
    include_once ("enc_dec.php");
    include_once ("config.php");
    
    $usrID = $_SESSION["usrID"];
    $action = NULL;

    if (isset($_POST["submit"])){
        $action = $_POST["submit"];
    }

    if (isset($_POST['sentDat'])){
        $action = $_POST['sentDat'];
    }

    $date = $GLOBALS['Date'];
    $encryTyp = "encrypt";
    $conn = NULL;
    $sql = NULL;
    $sql2 = NULL;
    $sql3 = NULL;
    $DirectLink = NULL;
    $result = NULL;

    //Open connection to DB if not open
    if(!isset($conn)){
        $conn = connectDB();
    }

    //Process Data and generate sql statement
    if(isset($action)){
        if($action == "AddUser"){
            $tmp = dec_enc($encryTyp,$_POST['email']);
            if (duplicateCheck("SELECT * FROM `users` WHERE USERS_EMAIL = '$tmp'",$conn)){
                    $data = array(
                        dec_enc($encryTyp,$_POST['email']),         //0
                        dec_enc($encryTyp,$_POST['pwd']),           //1
                        dec_enc($encryTyp,$_POST['uname']),         //2
                        $_POST['usrtype']                           //3
                        );

                    $sql = "INSERT INTO `users` (`USERS_ID`, `USERS_EMAIL`, `USERS_PASSWORD`, `USERS_NAME`, `USER_TYPE_ID`) VALUES (NULL, '$data[0]', '$data[1]', '$data[2]', '$data[3]')";
                    echo "<script> alert('User registered succesfully!'); </script>";
                    $DirectLink = $GLOBALS['dir'] . "admin.php?page=3";
            }else{
                echo "<script> alert('User with the email is already registered!'); </script>";
                $DirectLink = $GLOBALS['dir'] . "admin.php?page=3";
            }

        }elseif($action == "StudAddItem"){
            $studID = $_POST['studID'];

            if (duplicateCheck("SELECT * FROM `students` WHERE STUD_ID = $studID",$conn)){
                $data = array(
                    $studID,                                    //0
                    dec_enc($encryTyp,$_POST['studNme']),       //1
                    dec_enc($encryTyp,$_POST['studEmail']),     //2
                    dec_enc($encryTyp,$_POST['studPhone']),     //3
                    $_POST['item1'],                            //4
                    $_POST['item2'],                            //5
                    $_POST['item3']                             //6
                    );

                $sql = "INSERT INTO `students` (`STUD_ID`, `STUD_NME`, `STUD_EMAIL`, `STUD_PHONE`) VALUES ('$data[0]', '$data[1]', '$data[2]', '$data[3]')";
                $sql2 = "INSERT INTO `item_storage` (`ITEM_STORAGE_ID`, `ITEM_STORAGE_LISTS_ID_1`, `ITEM_STORAGE_LISTS_ID_2`, `ITEM_STORAGE_LISTS_ID_3`, `ITEM_STORAGE_STATUS`, `ITEM_STORAGE_DATE`, `ITEM_STUD_ID`, `ITEM_STORAGE_UPDATED_BY`) VALUES (NULL, '$data[4]', '$data[5]', '$data[6]', '1', '$date', '$studID', '1')";
                sessionLogout("login.html",false);

                echo "<script> alert('Item Succesfull Added!'); </script>";
                $DirectLink = $GLOBALS['dir'] . "index.html";

            }else{
                echo "<script> alert('Student ID is already registered!'); </script>";
                $DirectLink = $GLOBALS['dir'] . "student_registerItems.php";
            }

            
        }elseif($action == "adminSetting"){
            $data = array(
                $_POST['storageName'],  //0
                $_POST['dateStart'],    //1
                $_POST['dateEnd']       //2
            );

            $sql = "UPDATE `ss_config` SET `SS_CONFIG_VALUE` = '$data[1]' WHERE `ss_config`.`SS_CONFIG_ID` = 1";
            $sql2 = "UPDATE `ss_config` SET `SS_CONFIG_VALUE` = '$data[2]' WHERE `ss_config`.`SS_CONFIG_ID` = 2";
            $sql3 = "UPDATE `ss_config` SET `SS_CONFIG_VALUE` = '$data[0]' WHERE `ss_config`.`SS_CONFIG_ID` = 3";
            echo "<script>alert('Updated Successfully!')</script>";
            $DirectLink = $GLOBALS['dir'] . "admin.php";

        }elseif($action == "AddItems"){
            $data = $_POST['itemName'];
            if (duplicateCheck("SELECT * FROM `item_lists` WHERE ITEM_LISTS_NAME = '$data'",$conn)){
                $sql = "INSERT INTO `item_lists` (`ITEM_LISTS_ID`, `ITEM_LISTS_NAME`) VALUES (NULL, '$data')";
                echo "<script>alert('Item Added Successfully!')</script>";
                
            }else{
                echo "<script> alert('The item has already in database!'); </script>";
            }
            $DirectLink = $GLOBALS['dir'] . "admin.php?page=3";

        }elseif($action == "UpdateStatus"){
            $data = $_POST['newStatus'];
            $data = explode(";", $data);
            $sql = "UPDATE `item_storage` SET `ITEM_STORAGE_STATUS` = '$data[1]', `ITEM_STORAGE_UPDATED_BY` = $usrID WHERE `item_storage`.`ITEM_STORAGE_ID` = $data[0]";
            $DirectLink = $GLOBALS['dir'] . "admin.php?page=1";
        }

    }else{
        echo "[Data.php] : Variable action not set!";
    }

    //SQL
    if($sql != null){
       $result = SQL($sql,$conn,"SET");
    }

    if($sql2 != null){
       $result = SQL($sql2,$conn,"SET");
    }

    if($sql3 != null){
       $result = SQL($sql3,$conn,"SET");
    }

    //Direct user to url
    if($DirectLink != NULL){
        echo "<script> document.location.href='$DirectLink'; </script>";	
    }

    closeDB($conn);
?>