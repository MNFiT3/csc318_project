 <?php
    include "msg.php";
    include_once ("config.php");

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    if(empty($_SESSION['usrID']))
    {
        $redir = "login.php";
        $msg = sessionMsg();
        echo "<script> alert ('$msg'); document.location.href='$redir';</script>";
        //header('Location:index.php');
        //echo $_SESSION['usrID'];
    }

    function IsStudent(){
        if ($_SESSION['usrID'] == "Student"){
            session_unset();
            session_destroy();
            
            $redir = $GLOBALS['dir'] . "login.php";
            echo "<script> document.location.href='$redir';</script>";
        }
    }

    function sessionLogout($dirLocation,$alert){
        
        session_unset();
        session_destroy();

        if ($alert){
            $msg = logoutMsg();
            $redir = $GLOBALS["dir"] . $dirLocation;
            echo "<script> alert ('$msg'); document.location.href='$redir';</script>";
        }
    }
?>