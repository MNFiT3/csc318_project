<!--Created by ZeD-->
<?php include_once ("assets/php/session.php"); IsStudent(); ?>
        <div>
            <form method="post" action="assets/php/data.php">
                <div class="form-group">
                    <label>Email :</label>
                    <input name="email" type="email" class="form-control" required placeholder = "Enter your email" autocomplete="off"> 
                </div>
                <div class="form-group">
                    <label>Username :</label>
                    <input name="uname" type="text" class="form-control" required placeholder = "Enter your username" autocomplete="off" onkeypress="return validateStudName(event)"> 
                </div>
                <div class="form-group">
                    <label>Password :</label>
                    <input name="pwd" type="password" class="form-control" required placeholder = "Enter your password" autocomplete="off"> 
                </div>
                <div class="form-group">
                    <label>User Type :</label>
                    <select class="form-control" name = "usrtype">
                        <?php
                            $conn = connectDB();
                            $sql = "SELECT * FROM `user_type`";
                            $result = SQL($sql, $conn, "GET");

                            if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {
                                    $data = array($row['USER_TYPE_ID'], $row['USER_TYPE_DESC']);
                                    if($row['USER_TYPE_DESC'] != "STUDENT"){
                                        echo "<option value='$data[0]'>$data[1]</option>";
                                    }
                                }
                            }
                            closeDB($conn);
                        ?>
                    </select>
                </div>
                <button type="submit" name="submit" value="AddUser" class="btn btn-info">Add</button>
            </form>
        </div>
   