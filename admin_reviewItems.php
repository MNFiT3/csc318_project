<!-- Created by MN_FIT3 -->
<?php
    include_once "assets/php/dbc.php";
    include_once "assets/php/enc_dec.php";
    $action = 'decrypt';
    $conn = connectDB();
    $itemStatusID = 0;
    $usrTypeId = $_SESSION['usrTyp'];
    if ($usrTypeId == 0){
        $itemStatusID = 0;
    }elseif ($usrTypeId == 1){
        $itemStatusID = 1;
    }elseif($usrTypeId == 2){
        $itemStatusID = 2;
    }

    $sql = "
    SELECT 
	students.stud_id,
	students.stud_nme, 
	item_storage_id,
	B1.item_lists_name as 'item1',
	B2.item_lists_name as 'item2',
	B3.item_lists_name as 'item3',
	item_status.ITEM_STATUS_DSEC
        
    FROM 
        `item_storage`
        
    LEFT JOIN students on item_storage.ITEM_STUD_ID = students.STUD_ID
    LEFT JOIN item_lists B1 ON B1.ITEM_LISTS_ID = item_storage.ITEM_STORAGE_LISTS_ID_1
    LEFT JOIN item_lists B2 ON B2.ITEM_LISTS_ID = item_storage.ITEM_STORAGE_LISTS_ID_2
    LEFT JOIN item_lists B3 ON B3.ITEM_LISTS_ID = item_storage.ITEM_STORAGE_LISTS_ID_3
    LEFT JOIN item_status ON item_storage.item_storage_status = item_status.item_status_id

";
    if($itemStatusID != 0){
        $sql = $sql . "WHERE item_status_id = $itemStatusID";
    }


    $result = SQL($sql, $conn, "GET");
    

?>
        <div><!-- Content Start Here -->
            <table class="table">
                <thead>
                    <th>#</th>
                    <th>Student ID</th>
                    <th>Student Name</th>
                    <th>Item ID</th>
                    <th>Items</th>
                    <th>Status</th>
                    <th>Update Status</th>
                </thead>
                    <?php
                        $index = 1;
                        if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {
                                $data = array(
                                    $row['stud_id'],
                                    dec_enc($action,$row['stud_nme']),
                                    $row['item_storage_id'],
                                    $row['item1'],
                                    $row['item2'],
                                    $row['item3'],
                                    $row['ITEM_STATUS_DSEC']
                                );
                                echo "<tr>";
                                echo "<td>$index</td>";
                                echo "<td>$data[0]</td>";
                                echo "<td>$data[1]</td>";
                                echo "<td>$data[2]</td>";
                                echo "<td>$data[3], $data[4], $data[5]</td>";
                                echo "<td>$data[6]</td>";
                                echo "<td>
                                        <form method='post' action='assets/php/data.php'>
                                        <select name='newStatus' class='form-control'>";
                                            
                                if ($usrTypeId == 0){
                                    echo "<option value='$data[2];1'>Pending</option>";
                                    echo "<option value='$data[2];2'>Approved</option>";
                                    echo "<option value='$data[2];3'>Rejected</option>";
                                    echo "<option value='$data[2];4'>Stored</option>";
                                    echo "<option value='$data[2];5'>Returned</option>";
                                } elseif ($usrTypeId == 1){
                                    echo "<option value='$data[2];2'>Approve</option>";
                                    echo "<option value='$data[2];3'>Reject</option>"; 
                                } elseif ($usrTypeId == 2){
                                    echo "<option value='$data[2];4'>Stored</option>";
                                    echo "<option value='$data[2];5'>Returned</option>";  
                                }
                                echo   "</select>
                                        <br />
                                        <button name='submit' type='submit 'value='UpdateStatus' class='btn btn-info'>Update</button>
                                        </form>
                                        </td>";
                                $index++;
                            }
                            closeDB($conn);
                        }
                    ?>
                </tr>
            </table>
        </div><!-- Content End Here -->