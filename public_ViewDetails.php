<!-- Created by MN-FiT3 -->
<?php 
    $student = NULL;
    $storage = NULL;
    $studItm = NULL;

    if (isset($_GET['studID'])){
    if (true){
        $studID = $_GET['studID'];
        include_once ("assets/php/dbc.php");
        include_once ("assets/php/enc_dec.php");
        include_once ("assets/php/SSconfigLoader.php");
        $conn = connectDB();

        $sql = "
                SELECT
				students.STUD_NME, 
                students.STUD_ID,
                students.STUD_PHONE,
                students.STUD_EMAIL,
                B1.item_lists_name as 'item1',
                B2.item_lists_name as 'item2',
                B3.item_lists_name as 'item3',
                item_storage_id,
                item_status.ITEM_STATUS_DSEC
                    
                FROM 
                    `item_storage`
                    
                LEFT JOIN students on item_storage.ITEM_STUD_ID = students.STUD_ID
                LEFT JOIN item_lists B1 ON B1.ITEM_LISTS_ID = item_storage.ITEM_STORAGE_LISTS_ID_1
                LEFT JOIN item_lists B2 ON B2.ITEM_LISTS_ID = item_storage.ITEM_STORAGE_LISTS_ID_2
                LEFT JOIN item_lists B3 ON B3.ITEM_LISTS_ID = item_storage.ITEM_STORAGE_LISTS_ID_3
                LEFT JOIN item_status ON item_storage.item_storage_status = item_status.item_status_id

                WHERE
                    students.stud_id = $studID
        ";

        $result = SQL($sql,$conn,"GET");
        $encryTyp = "decrypt";
        $student = NULL;

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $student = array(
                $row['STUD_ID'],
                dec_enc($encryTyp,$row['STUD_NME']),
                dec_enc($encryTyp,$row['STUD_EMAIL']),
                dec_enc($encryTyp,$row['STUD_PHONE']),
                $row['item1'],
                $row['item2'],
                $row['item3'],
                $row['item_storage_id'],
                $row['ITEM_STATUS_DSEC']
            );
        }else{
            echo "<script> alert ('Student ID not exist!'); document.location.href='ViewStatus.html';</script>";
        }

        }
    }
?>

<html>
    <head>
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
    </head>
    <body class="container">
        <!-- Content Start Here -->
        <br />
        <?php
            $ownDir = $_SERVER['PHP_SELF']."?print=1&studID=$studID";
            if (isset($_GET['print'])){
                echo "<script>window.print();</script>";
            }else{
                echo "<a class='btn' href='$ownDir'>Print</a>";
            }

        ?>
        <div>
            <hr />
            <div id="qrcode" ></div>
            <hr />
            <div>
                <div class="row">
                    <div class="col-md-2">
                        <b>Name </b>
                    </div>
                    <div class="col-md-4">
                        <?php echo $student[1];  ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <b>Student ID </b>
                    </div>
                    <div class="col-md-4">
                        <?php echo $student[0];  ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <b>Phone No</b>
                    </div>
                    <div class="col-md-4">
                        <?php echo $student[3];  ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <b>Email</b>
                    </div>
                    <div class="col-md-4">
                        <?php echo $student[2];  ?>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="col-md-2">
                        <b>Status</b>
                    </div>
                    <div class="col-md-4">
                        <!-- div class="ss-status-green">Accepted</div -->
                        <div ><?php echo $student[8]; ?></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <b>Storage Name</b>
                    </div>
                    <div class="col-md-4">
                        <?php echo $GLOBALS['Room_Name']; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <b>Store Date</b>
                    </div>
                    <div class="col-md-4">
                        <?php echo $GLOBALS['Stored_date']; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                       <b>Retrive Date</b>
                    </div>
                    <div class="col-md-4">
                        <?php echo $GLOBALS['Returned_date']; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <b>Price (RM)</b>
                    </div>
                    <div class="col-md-4">
                        <?php echo $GLOBALS['PRICE'] ?>
                    </div>
                </div>
                <hr />
                <table class="table" width="100%">
                    <thead style="color:black;">
                        <th>#</th>
                        <th class="col-md-4">Item</th>
                    </thead>
                    <tr>
                        <th scope="row" >1</th>
                        <td><?php echo $student[4];  ?></td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td><?php echo $student[5];  ?></td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td><?php echo $student[6];  ?></td>
                    </tr>
                </table>
            </div>
            <br />
        </div>
        <!-- Content End Here -->
        <script src="assets/plugins/jquery-3.2.1.min.js"></script>
        <script src="assets/js/tether.min.js"></script>
        <script src="assets/js/bootstrap.js"></script>
        <script src="assets/plugins/qrcode.js"></script>
        <script type="text/javascript">
            var qrcode = new QRCode("qrcode", {
                text: <?php echo "'" . $studID . "::" . $student[7] . "'"; ?>,
                width: 128,
                height: 128,
                colorDark : "#000000",
                colorLight : "#ffffff",
                correctLevel : QRCode.CorrectLevel.H
            });
        </script>
    </body>
</html>