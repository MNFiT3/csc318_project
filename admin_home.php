<!-- Created by MN-FiT3 -->
<?php
    $dir = "assets/php/";
    include_once ($dir . "session.php");
    include_once ($dir . "dbc.php");
    include_once ($dir . "SSconfigLoader.php");

    $conn = connectDB();

    $sql = "SELECT ITEM_STATUS_DESC FROM  `item_status`";
    $result = SQL($sql,$conn,"GET");

    $Tdata = 0;
    $data = array(0,0,0,0,0);
    for ($x = 0; $x < 5; $x++){
        $dataID = ($x + 1); 
        $sql = "SELECT COUNT(ITEM_STORAGE_STATUS) AS 'total' FROM `item_storage` WHERE ITEM_STORAGE_STATUS = $dataID";
        $result = SQL($sql, $conn, "GET");
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $data[$x]=$row['total'];
            $Tdata = $Tdata + $data[$x];
        }
    }

    //For Refrence
    /* ITEM_STATUS  - ARRAY_INDEX_IN_$data
    *  PENDING      - 0
    *  APPROVED     - 1
    *  REJECTED     - 2
    *  STORED       - 3
    *  RETURNED     - 4
    */
    

?>
        <div><!-- Content Start Here -->
            <h1 style="">Welcome <?php echo $_SESSION['usrNme']; ?></h1>
            <hr />
            
            <div class="row">
                <div class="col ss-card">
                    <br />
                    <label><u>Current UPK Details</u></label>
                    <div class="form-group">
                        <label>Storage Name</label>
                        <?php $temp = $GLOBALS['Room_Name']; echo "<input class='form-control' type='text' value='$temp' disabled>"; ?>
                    </div>
                    <div class="form-group">
                        <label>Start Date</label>
                        <?php $temp = $GLOBALS['Stored_date']; echo "<input class='form-control' type='text' value='$temp' disabled>"; ?>
                    </div>
                    <div class="form-group">
                        <label>End Date</label>
                        <?php $temp = $GLOBALS['Returned_date']; echo "<input class='form-control' type='text' value='$temp' disabled>"; ?>
                    </div>
                    <div class="form-group">
                        <label>Price</label>
                        <?php $temp = $GLOBALS['PRICE']; echo "<input class='form-control' type='text' value='RM $temp' disabled>"; ?>
                    </div>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col col-md-6 ss-card">
                    <br />
                    <label><u>Current Item Status</u></label>
                    <div class="form-group">
                        <label>Pending</label>
                        <?php echo "<input class='form-control' type='text' value='$data[0]' disabled>"; ?>
                    </div>
                    <div class="form-group">
                        <label>Approved</label>
                        <?php echo "<input class='form-control' type='text' value='$data[1]' disabled>"; ?>
                    </div>
                    <div class="form-group">
                        <label>Rejected</label>
                        <?php echo "<input class='form-control' type='text' value='$data[2]' disabled>"; ?>
                    </div>
                    <div class="form-group">
                        <label>Stored</label>
                        <?php echo "<input class='form-control' type='text' value='$data[3]' disabled>"; ?>
                    </div>
                    <div class="form-group">
                        <label>Returned</label>
                        <?php echo "<input class='form-control' type='text' value='$data[4]' disabled>"; ?>
                    </div>
                    <div class="form-group">
                        <label>Total</label>
                        <?php echo "<input class='form-control' type='text' value='$Tdata' disabled>"; ?>
                    </div>
                </div>
                <div class="col col-sm-1"></div>
                <div class="col col-sx-6 ss-card">
                    <br /><br />
                    <canvas id="myChart" width="400" height="400"></canvas>
                </div>
            </div>
            
        </div><!-- Content End Here -->
        <script src="assets/js/Chart.bundle.min.js"></script>
        <script>
            var ctx = document.getElementById("myChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["Pending", "Approved", "Rejected", "Stored", "Returned"],
                    datasets: [{
                        label: '# of Students',
                        <?php echo "data: [$data[0], $data[1], $data[2], $data[3], $data[4]],"; ?>
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
        </script>