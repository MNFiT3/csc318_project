<?php
    session_start();
    if (isset($_POST['email'])){
        include_once "assets/php/dbc.php";
        include_once "assets/php/enc_dec.php";
        $enc = "encrypt";
        $conn = connectDB();
        $data = array(dec_enc($enc, $_POST['email']), dec_enc($enc, $_POST['password']));
        $sql = "SELECT * FROM `users` WHERE USERS_EMAIL = '$data[0]' AND USERS_PASSWORD = '$data[1]'";
        $result = SQL($sql, $conn, "GET");
        if ($result->num_rows > 0){
            $row = $result->fetch_assoc();
            $_SESSION['usrID'] = $row['USERS_ID'];
            $_SESSION['usrTyp'] = $row['USER_TYPE_ID'];
            $_SESSION['usrNme'] = dec_enc("decrypt", $row['USERS_NAME']);
            closeDB($conn);
            include_once ("assets/php/SSconfigLoader.php");
            header("Location: " . $GLOBALS['dir'] . "admin.php?page=home");
        }else{
            echo "<script>alert('Wrong Email or Password')</script>";
        }
    }
?>

<!-- Created by MN-FiT3 -->
<html>
    <head>
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
    </head>
    <body>
    <nav class="navbar navbar-inverse bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">UPK Storage System</a>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="index.html" style="color:white">Homepage</a>
                </li>
            </ul>
        </div>
        </nav>
    <div class="container">
        <!-- Content Start Here -->
        <div>
            <br>
            <h1>Login</h1>
            <hr />
            <form method="post" action="login.php">
                <div class="form-group">
                    <label>Email</label>
                    <input name="email" type="text" class="form-control" autocomplete="off" required> 
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input name="password" type="password" class="form-control" min="8" required> 
                </div>
                <br>
                <button type="submit" name="submit" value="login" class="btn btn-info">Login</button>
            </form>
            <br>
        </div>
        <!-- Content End Here -->
        <script src="assets/plugins/jquery-3.2.1.min.js"></script>
        <script src="assets/js/tether.min.js"></script>
        <script src="assets/js/bootstrap.js"></script>
        </div>
    </body>
</html>