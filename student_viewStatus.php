<!-- Created by MN-FiT3 -->
<html>
    <head>
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
    </head>
    <body class="container">
        <br />
        <div class="ss-card"><!-- Content Start Here -->
            <div class="container">
                <h3>View Items Status</h3>
                <hr />
                <form name="studSearch" method="post" action="assets/php/data.php">
                    <div class="form-group">
                        <label>Student ID</label>
                        <input name="studID" type="text" class="form-control" autocomplete="off" placeholder="Student ID">
                        <br />
                        <button name="submit" type="submit" value="ViewItemStatus" class="btn btn-success">Search</button>
                        <br />
                    </div>
                </form>
                <hr />
            </div>        
        </div><!-- Content End Here -->
        <br />
        <script src="assets/plugins/jquery-3.2.1.min.js"></script>
        <script src="assets/js/tether.min.js"></script>
        <script src="assets/js/bootstrap.js"></script>
    </body>
</html>