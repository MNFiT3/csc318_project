<!-- Created by MN-FIT3 -->
<?php 
    include_once "assets/php/session.php";
    include_once "assets/php/dbc.php";
    include_once "assets/php/enc_dec.php";

    $encryTyp = "encrypt";

    if (isset($_POST['oldPass'])){
        $data = array(dec_enc($encryTyp,$_POST['oldPass']),dec_enc($encryTyp,$_POST['newPass']));
        $userID = $_SESSION['usrID'];
        $conn = connectDB();
        $sql = "SELECT * FROM USERS WHERE USERS_ID = $userID AND USERS_PASSWORD = '$data[0]'";
        $result = SQL($sql,$conn,"GET");

        if ($result->num_rows > 0) {
            $sql = "UPDATE `users` SET `USERS_PASSWORD` = '$data[1]' WHERE `users`.`USERS_ID` = $userID";
            SQL($sql,$conn,"SET");

            echo "<script> alert ('Password Changed Succesfully!'); document.location.href='admin.php?page=5';</script>";
        }else{
            echo "<script> alert ('Wrong Old Password!'); document.location.href='admin.php?page=5';</script>";
        }
    }

?>
        <div><!-- Content Start Here -->
            <form name="AdminChangePassword" method="post" action="admin_changePassword.php">
                <div class="form-group">
                    <label>Old Password</label>
                    <input id="oldPass" name="oldPass" type="password" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>New Password</label>
                    <input id="newPass" name="newPass" type="password" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Re-Type New Password</label>
                    <input id="reNewPass" name="reNewPass" type="password" class="form-control" required>
                </div>
                <button name="submit" type="submit" value="AdminChangePassword" class="btn btn-success">Update Password</button>
            </form>
        </div><!-- Content End Here -->