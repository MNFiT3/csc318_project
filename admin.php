<!-- Created by MN-FiT3 -->
<?php include_once "assets/php/session.php"; include_once "assets/php/dbc.php"; IsStudent();?>
<html>
    <head>
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
        <script src="assets/custom/upk-ss.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-inverse bg-inverse">
        <!-- Navbar content -->
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">UPK Storage System</a>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="admin.php?page=home">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin.php?page=1">Review Status</a>
                </li>
                <?php
                    $usrTyp = $_SESSION['usrTyp'];
                    if ($usrTyp == '0'){
                        echo '<li class="nav-item"><a class="nav-link" href="admin.php?page=3">UPK-SS Setting</a></li>';
                    }
                ?>
                <li class="nav-item">
                    <a class="nav-link" href="admin.php?page=5">Account Setting</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin.php?page=6">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin.php?page=logout">Logout</a>
                </li>
            </ul>
        </div>
        </nav>
        <br />
        <div class="container"><!-- Content Start Here -->
            <?php
                if (isset($_GET['page'])){
                    $page = $_GET['page'];
                    if ($page == 1){
                        include_once "admin_reviewItems.php";
                    }elseif ($page == 3){
                        include_once "admin_SettingMenu.php";
                    }elseif ($page == 4){
                        
                    }elseif ($page == 5){
                        include_once("admin_changePassword.php");
                    }elseif ($page == 6){
                        include_once("admin_help.html");
                    }elseif ($page == "home"){
                        include_once("admin_home.php");
                    }else{
                        sessionLogout("index.html", true);
                    }
                }
            ?>
        </div><!-- Content End Here -->
        <br />
        <script src="assets/plugins/jquery-3.2.1.min.js"></script>
        <script src="assets/js/tether.min.js"></script>
        <script src="assets/js/bootstrap.js"></script>
    </body>
</html>