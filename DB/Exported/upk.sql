-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2017 at 08:00 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `upk`
--

-- --------------------------------------------------------

--
-- Table structure for table `item_lists`
--

CREATE TABLE `item_lists` (
  `ITEM_LISTS_ID` int(11) NOT NULL,
  `ITEM_LISTS_NAME` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_lists`
--

INSERT INTO `item_lists` (`ITEM_LISTS_ID`, `ITEM_LISTS_NAME`) VALUES
(1, 'BAG'),
(2, 'COMPUTER'),
(3, 'BOOKS'),
(4, 'MONITOR'),
(5, 'BROOM'),
(6, 'BICYCLE');

-- --------------------------------------------------------

--
-- Table structure for table `item_status`
--

CREATE TABLE `item_status` (
  `ITEM_STATUS_ID` int(11) NOT NULL,
  `ITEM_STATUS_DSEC` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_status`
--

INSERT INTO `item_status` (`ITEM_STATUS_ID`, `ITEM_STATUS_DSEC`) VALUES
(1, 'PENDING'),
(2, 'APPROVED'),
(3, 'REJECTED'),
(4, 'STORED'),
(5, 'RETURNED'),
(6, 'EXPIRED');

-- --------------------------------------------------------

--
-- Table structure for table `item_storage`
--

CREATE TABLE `item_storage` (
  `ITEM_STORAGE_ID` int(11) NOT NULL,
  `ITEM_STORAGE_LISTS_ID_1` int(11) NOT NULL,
  `ITEM_STORAGE_LISTS_ID_2` int(11) NOT NULL,
  `ITEM_STORAGE_LISTS_ID_3` int(11) NOT NULL,
  `ITEM_STORAGE_STATUS` int(11) NOT NULL,
  `ITEM_STORAGE_DATE` date NOT NULL,
  `ITEM_STUD_ID` int(11) NOT NULL,
  `ITEM_STORAGE_UPDATED_BY` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_storage`
--

INSERT INTO `item_storage` (`ITEM_STORAGE_ID`, `ITEM_STORAGE_LISTS_ID_1`, `ITEM_STORAGE_LISTS_ID_2`, `ITEM_STORAGE_LISTS_ID_3`, `ITEM_STORAGE_STATUS`, `ITEM_STORAGE_DATE`, `ITEM_STUD_ID`, `ITEM_STORAGE_UPDATED_BY`) VALUES
(1, 2, 4, 3, 4, '2017-10-19', 2015845492, 5),
(2, 6, 3, 3, 5, '2017-10-19', 20000, 1),
(3, 1, 1, 1, 4, '2017-11-04', 21, 1),
(4, 1, 1, 1, 4, '2017-11-04', 321, 1),
(5, 1, 1, 1, 4, '2017-11-04', 435, 1),
(6, 4, 6, 4, 2, '2017-11-16', 2121, 6),
(7, 1, 1, 1, 2, '2017-11-21', 2313, 6),
(8, 1, 1, 1, 2, '2017-11-21', 55555555, 6),
(9, 6, 2, 1, 1, '2017-11-23', 201554545, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ss_config`
--

CREATE TABLE `ss_config` (
  `SS_CONFIG_ID` int(11) NOT NULL,
  `SS_CONFIG_NAME` varchar(128) NOT NULL,
  `SS_CONFIG_VALUE` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ss_config`
--

INSERT INTO `ss_config` (`SS_CONFIG_ID`, `SS_CONFIG_NAME`, `SS_CONFIG_VALUE`) VALUES
(1, 'DATE_TOBE_STORED', '2017-12-31'),
(2, 'DATE_TOBE_RETURNED', '2018-12-31'),
(3, 'ROOM_NAME', 'BILIK SEBERGUNA A'),
(4, 'PRICE', '12.00');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `STUD_ID` int(11) NOT NULL,
  `STUD_NME` varchar(255) NOT NULL,
  `STUD_EMAIL` varchar(255) NOT NULL,
  `STUD_PHONE` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`STUD_ID`, `STUD_NME`, `STUD_EMAIL`, `STUD_PHONE`) VALUES
(21, 'd3drVDBKNGYyZVdtY2tWaGlrMW40dz09', 'M3dPRzFGUTBVRlRuNTRWeVZtZ2NiZz09', 'd3drVDBKNGYyZVdtY2tWaGlrMW40dz09'),
(321, 'YzlaQ3hFRmZBZEFldi81S1VtbmVBZz09', 'WmMxdGo3TU9sa1g2dUpOa3FZY09kdz09', 'YzlaQ3hFRmZBZEFldi81S1VtbmVBZz09'),
(435, 'bi9YVysxYUhWd2NSeGEyc0ZnUHdldz09', 'Z2tVblZLa2FaWlJOZHFNMWI1dnREdz09', 'azFQOHBaOGlYcTFEMVl3dGhqRVNRUT09'),
(2121, 'VWpNLzZiS0hSa0l6UWk2c09yejZ3dz09', 'UU96alQ4K1dSazE2QWhlTlBRa0VRUT09', 'SWN5WkdDYXlOUTBaR05WSHNNemJIZz09'),
(2313, 'b05vZmFUMklIVWxxWVQyZ3NmR0tNUT09', 'b05vZmFUMklIVWxxWVQyZ3NmR0tNUT09', 'b05vZmFUMklIVWxxWVQyZ3NmR0tNUT09'),
(20000, 'N0NhK2J6R0tpdjV4aDBoN2Q4Titndz09', 'bFBrREJkYWhXYzBrVEJLSDhQZVFSQT09', 'bjFYZ0cxQ3JCNHphMG13bzNEbkJGZz09'),
(55555555, 'anQzOXlPNXljWVUzeUg4N2ZFK3dFQT09', 'SHFGYzh6WlkwU3N5MlZwVDFiREJ0MytnamRqTXFXMW04M2NRZVFiWkhPdz0=', 'anQzOXlPNXljWVUzeUg4N2ZFK3dFQT09'),
(201554545, 'R1ZIMWhvR1hpL2JVMHdPQXVZRENDUT09', 'WVhoTGtVNElBRWlNR1BVWEJiMVlxUFVCUEl5WEhZdUxwVU16VFY4M1Vwbz0=', 'eHovS2EwMjc1dDErdG5DYnNueVZ0QT09'),
(2015845492, 'ajNTdG1McjQ3d1pTZVIrck9UbjlOV0x1NGI0eXg2N2RNbWd0QnpJbW1uS2VWV1oxNVNYTGZseUJXeXVib0hQWg==', 'Nm5RTEtQNUNiMlJzTDl4VjVoV2MxQi9KN0ZRZFdLOTZXSXA5TmV5cSs2Zz0=', 'dnBTWEwvbTZQakRXdmJDQ2xXNzg5dz09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `USERS_ID` int(11) NOT NULL,
  `USERS_EMAIL` varchar(255) NOT NULL,
  `USERS_PASSWORD` varchar(255) NOT NULL,
  `USERS_NAME` varchar(255) NOT NULL,
  `USER_TYPE_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`USERS_ID`, `USERS_EMAIL`, `USERS_PASSWORD`, `USERS_NAME`, `USER_TYPE_ID`) VALUES
(1, 'Z2EzcTB4d1MrMlUxVGFuUjY5QUxNdz09', 'L2VUZVh5UlZNWWhoaitrVGRrNGQ5Zz09', 'L2VUZVh5UlZNWWhoaitrVGRrNGQ5Zz09', 3),
(2, 'L2VUZVh5UlZNWWhoaitrVGRrNGQ5Zz09', 'L2VUZVh5UlZNWWhoaitrVGRrNGQ5Zz09', 'L2VUZVh5UlZNWWhoaitrVGRrNGQ5Zz09', 0),
(4, 'dFQzL0duQ2I1RmdTdXNiVit5WWYvUT09', 'L3F4c09CTWtKWnNpZUFBZUVWbXlsdz09', 'L3F4c09CTWtKWnNpZUFBZUVWbXlsdz09', 1),
(5, 'ZzVhc3k5aWd1WCtrbkJFRm43ODJ2QT09', 'WUN6OGJmRkZ1TUJzOVV6dU5NNGJUQT09', 'WUN6OGJmRkZ1TUJzOVV6dU5NNGJUQT09', 2),
(6, 'WEN6Zitua09TQlJwUlZkc08xL2crWmxNNGMxR21GVjQ2N21UZ1lzRXowaz0=', 'ZVpITXhQWnVsclMwMGtDQkJ3ZkJlUT09', 'ZVpITXhQWnVsclMwMGtDQkJ3ZkJlUT09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `USER_TYPE_ID` int(11) NOT NULL,
  `USER_TYPE_DESC` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`USER_TYPE_ID`, `USER_TYPE_DESC`) VALUES
(0, 'ADMIN ACCESS LEVEL 0'),
(1, 'ADMIN ACCESS LEVEL 1'),
(2, 'ADMIN ACCESS LEVEL 2'),
(3, 'STUDENT');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `item_lists`
--
ALTER TABLE `item_lists`
  ADD PRIMARY KEY (`ITEM_LISTS_ID`);

--
-- Indexes for table `item_status`
--
ALTER TABLE `item_status`
  ADD PRIMARY KEY (`ITEM_STATUS_ID`);

--
-- Indexes for table `item_storage`
--
ALTER TABLE `item_storage`
  ADD PRIMARY KEY (`ITEM_STORAGE_ID`),
  ADD KEY `ITEM_STORAGE_LISTS_ID_1` (`ITEM_STORAGE_LISTS_ID_1`),
  ADD KEY `ITEM_STORAGE_LISTS_ID_2` (`ITEM_STORAGE_LISTS_ID_2`),
  ADD KEY `ITEM_STORAGE_LISTS_ID_3` (`ITEM_STORAGE_LISTS_ID_3`),
  ADD KEY `ITEM_STORAGE_STATUS` (`ITEM_STORAGE_STATUS`),
  ADD KEY `ITEM_STUD_ID` (`ITEM_STUD_ID`),
  ADD KEY `ITEM_STORAGE_UPDATED_BY` (`ITEM_STORAGE_UPDATED_BY`);

--
-- Indexes for table `ss_config`
--
ALTER TABLE `ss_config`
  ADD PRIMARY KEY (`SS_CONFIG_ID`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`STUD_ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`USERS_ID`),
  ADD KEY `USER_TYPE_ID` (`USER_TYPE_ID`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`USER_TYPE_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `item_lists`
--
ALTER TABLE `item_lists`
  MODIFY `ITEM_LISTS_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `item_status`
--
ALTER TABLE `item_status`
  MODIFY `ITEM_STATUS_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `item_storage`
--
ALTER TABLE `item_storage`
  MODIFY `ITEM_STORAGE_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `ss_config`
--
ALTER TABLE `ss_config`
  MODIFY `SS_CONFIG_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `USERS_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `item_storage`
--
ALTER TABLE `item_storage`
  ADD CONSTRAINT `item_storage_ibfk_1` FOREIGN KEY (`ITEM_STORAGE_LISTS_ID_1`) REFERENCES `item_lists` (`ITEM_LISTS_ID`),
  ADD CONSTRAINT `item_storage_ibfk_2` FOREIGN KEY (`ITEM_STORAGE_LISTS_ID_2`) REFERENCES `item_lists` (`ITEM_LISTS_ID`),
  ADD CONSTRAINT `item_storage_ibfk_3` FOREIGN KEY (`ITEM_STORAGE_LISTS_ID_3`) REFERENCES `item_lists` (`ITEM_LISTS_ID`),
  ADD CONSTRAINT `item_storage_ibfk_4` FOREIGN KEY (`ITEM_STORAGE_STATUS`) REFERENCES `item_status` (`ITEM_STATUS_ID`),
  ADD CONSTRAINT `item_storage_ibfk_5` FOREIGN KEY (`ITEM_STUD_ID`) REFERENCES `students` (`STUD_ID`),
  ADD CONSTRAINT `item_storage_ibfk_6` FOREIGN KEY (`ITEM_STORAGE_UPDATED_BY`) REFERENCES `users` (`USERS_ID`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`USER_TYPE_ID`) REFERENCES `user_type` (`USER_TYPE_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
